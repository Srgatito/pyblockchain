# -*- coding: utf-8 -*-
"""
Created on Sun May 24 04:20:22 2020

@author: Rafa
"""


from django.http import HttpResponse
import datetime

def current_datetime(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)